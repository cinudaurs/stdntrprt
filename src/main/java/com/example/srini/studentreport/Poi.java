package com.example.srini.studentreport;

/**
 * Created by srini on 8/18/16.
 */
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;


public class Poi {

//    /**
//     * @param args the command line arguments
//     */
////    public static void main(String[] args) throws IOException {
////        // TODO code application logic here
////
////      //  JsonArray arr = leerJson("stdntrprt.json");
////
////        ArrayList<StudentReport> menu=createStdRprt(arr);
////        generateExcel(menu);
////
////    }


    public static void generateExcel(ArrayList<StudentReport> studentReport, File filename) throws FileNotFoundException, IOException{


        String subject = null;
        String attendance = null;

        Workbook wb = new HSSFWorkbook();

        Sheet sheet = wb.createSheet("student report");

        Row row = sheet.createRow(0);

        Cell cell0 = row.createCell(0);
        Cell cell1 = row.createCell(1);
        Cell cell2 = row.createCell(2);
        Cell cell3 = row.createCell(3);
        Cell cell4 = row.createCell(4);
        Cell cell5 = row.createCell(5);
        Cell cell6 = row.createCell(6);
        //      Cell cell7 = row.createCell(7);

        cell0.setCellValue("date");
        cell1.setCellValue("studentId");
        cell2.setCellValue("studentName");
        cell3.setCellValue("academicYear");
        cell4.setCellValue("semister");
        cell5.setCellValue("subjects");
        cell6.setCellValue("attendance");
        //     cell7.setCellValue("totalLectures");

        int subjectsLength = 0;

        //assumes the last element in the json array is the json element with just the key of totalLectures.
        int studentReportLength = studentReport.size()-1;

        for(int m=0;m < studentReportLength; m++){

            subjectsLength += studentReport.get(m).getSubjects().length;

        }

        int n = 0;
        int i = 1;

        while ( n < studentReportLength) {

            while (i < subjectsLength) {

                int j = studentReport.get(n).getSubjects().length;

                Row row1 = sheet.createRow(i);
                Cell ca = row1.createCell(0);
                ca.setCellValue(studentReport.get(n).getDate());

                Cell cb = row1.createCell(1);
                cb.setCellValue(studentReport.get(n).getStudentId());

                Cell cc = row1.createCell(2);
                cc.setCellValue(studentReport.get(n).getStudentName());

                Cell cd = row1.createCell(3);
                cd.setCellValue(studentReport.get(n).getAcademicYear());

                Cell ce = row1.createCell(4);
                ce.setCellValue(studentReport.get(n).getSemister());

                Cell cf = row1.createCell(5);
                cf.setCellValue(studentReport.get(n).getSubjects()[0]);

                Cell cg = row1.createCell(6);
                cg.setCellValue(studentReport.get(n).getAttendance()[0]);

                int k = 1;

                while (k < j) {

                    Row embedRowk = sheet.createRow(i + k);
                    Cell embedCellSubj = embedRowk.createCell(5);

                    Cell embedCellAtt = embedRowk.createCell(6);

                    embedCellSubj.setCellValue(studentReport.get(n).getSubjects()[k]);

                    embedCellAtt.setCellValue(studentReport.get(n).getAttendance()[k]);

                    k += 1;

                }

                k = k - 1;

                i += j;


                Row totalRow = sheet.createRow(i);
                Cell totalCell = totalRow.createCell(4);
                totalCell.setCellValue("Total Lectures");

                Cell totalCellVal = totalRow.createCell(6);
                totalCellVal.setCellValue(studentReport.get(n).getTotalLectures());

                i+=1;

            }


            n += 1;

        }


        if (studentReport.get(n).getFinalTotalLectures() != null ){

            String finalTotalLecturesVal = studentReport.get(n).getFinalTotalLectures();

            Row totalRow = sheet.createRow(i);
            Cell totalCell = totalRow.createCell(4);
            totalCell.setCellValue("Total Lectures");

            Cell totalCellVal = totalRow.createCell(6);
            totalCellVal.setCellValue(studentReport.get(n).getFinalTotalLectures());


        }




//        sheet. autoSizeColumn(7);


        FileOutputStream fileOut = new FileOutputStream(filename);
        wb.write(fileOut);

        System.out.println("excel generated");
        fileOut.close();

    }
    public static ArrayList<StudentReport> createStdRprt(JSONArray arr){

        ArrayList<StudentReport> menu = new  ArrayList<StudentReport>();

        for (int i = 0; i < arr.length()-1; i++) {

            StudentReport studentReport = new StudentReport();

            JSONObject json3 = null;
            try {

            json3 = arr.getJSONObject(i);



            //JsonElement  dateElement = json3.get("date");

                String date = json3.getString("date");
                studentReport.setDate(date);

            //JsonElement  studentIdElement = json3.get("studentId");


            String studentId = json3.getString("studentId");
            studentReport.setStudentId(studentId);

            //JsonElement studentNameElement =json3.get("studentName");
            String studentName = json3.getString("studentName");
            studentReport.setStudentName(studentName);

            //JsonElement academicYeareElement =json3.get("academicYear");
            String academicYear = json3.getString("academicYear");
            studentReport.setAcademicYear(academicYear);


            //JsonElement semisterElement =json3.get("semister");

            String semister = json3.getString("semister");
            studentReport.setSemister(semister);


           // JsonElement  subjectsElement = json3.get("subjects");

            JSONArray subjectsArray = json3.getJSONArray("subjects");
            String[] subjects = new String[subjectsArray.length()];
            for (int j = 0; j < subjectsArray.length(); j++){
                subjects[j]= subjectsArray.get(j).toString();
            }
            studentReport.setSubjects(subjects);


           // JsonElement  attendanceElement = json3.get("attendance");
            JSONArray attendanceArray = json3.getJSONArray("attendance");
            String[] attendance = new String[attendanceArray.length()];
            for (int j = 0; j < attendanceArray.length(); j++){
                attendance[j]= attendanceArray.get(j).toString();
            }
            studentReport.setAttendance(attendance);


            //JsonElement totalLecturesElement =json3.get("totalLectures");
            String totalLectures = json3.getString("totalLectures");
            studentReport.setTotalLectures(totalLectures);

            menu.add(studentReport);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        }


        JSONObject jsonfinal = null;
        try {
            jsonfinal = arr.getJSONObject(arr.length()-1);

         String finalTotalLectures = jsonfinal.getString("totalLectures");

        StudentReport studentReport = new StudentReport();
        studentReport.setFinalTotalLectures(finalTotalLectures);



        menu.add(studentReport);
        } catch (JSONException e) {
            e.printStackTrace();
        }



        return menu;
    }

    public static JsonArray leerJson(String file) {

        JsonArray jsonArray = new JsonArray();

        try {
            JsonParser parser = new JsonParser();
            JsonElement jsonElement = parser.parse(new FileReader(file));
            jsonArray = jsonElement.getAsJsonArray();
        } catch (FileNotFoundException e) {

        } catch (IOException ioe) {

        }
        return jsonArray;
    }
}
