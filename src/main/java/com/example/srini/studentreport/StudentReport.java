package com.example.srini.studentreport;

/**
 * Created by srini on 8/18/16.
 */
public class StudentReport {

    String date;
    String studentId;
    String studentName;
    String academicYear;
    String semister;

    String[]subjects;
    String[] attendance;

    String totalLectures;

    String finalTotalLectures;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getAcademicYear() {
        return academicYear;
    }

    public void setAcademicYear(String academicYear) {
        this.academicYear = academicYear;
    }

    public String getSemister() {
        return semister;
    }

    public void setSemister(String semister) {
        this.semister = semister;
    }

    public String[] getSubjects() {
        return subjects;
    }

    public void setSubjects(String[] subjects) {
        this.subjects = subjects;
    }

    public String[] getAttendance() {
        return attendance;
    }

    public void setAttendance(String[] attendance) {
        this.attendance = attendance;
    }

    public String getTotalLectures() {
        return totalLectures;
    }

    public void setTotalLectures(String totalLectures) {
        this.totalLectures = totalLectures;
    }



    public String getFinalTotalLectures() {
        return finalTotalLectures;
    }

    public void setFinalTotalLectures(String finalTotalLectures) {
        this.finalTotalLectures = finalTotalLectures;
    }



}
