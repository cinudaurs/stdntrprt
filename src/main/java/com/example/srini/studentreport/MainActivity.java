package com.example.srini.studentreport;

import android.Manifest;
import android.app.Activity;
import android.app.ListActivity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import android.support.design.widget.FloatingActionButton;
import android.widget.Toast;

import com.google.gson.JsonArray;


import static com.example.srini.studentreport.Poi.createStdRprt;
import static com.example.srini.studentreport.Poi.generateExcel;

public class MainActivity extends ListActivity {


    private static final String FINALTOTALLECTURES = "finalTotalLecturesVal";
    private Context context;
//    private static JSONArray jarray = ""

    private static final String DATE = "date";
    private static final String STUDENTID = "studentId";
    private static final String STUDENTNAME = "studentName";
    private static final String ACADEMICYEAR = "academicYear";
    private static final String SEMISTER = "semister";
    private static final String SUBJECTS = "subjects";
    private static final String ATTENDANCE = "attendance";
    private static final String TOTALLECTURES = "totalLectures";

    private static final String SUBJ1 = "subj1";
    private static final String SUBJ2 = "subj2";
    private static final String SUBJ3 = "subj3";
    private static final String SUBJ4 = "subj4";
    private static final String SUBJ5 = "subj5";
    private static final String SUBJ6 = "subj6";


    private static final String ATT1 = "att1";
    private static final String ATT2 = "att2";
    private static final String ATT3 = "att3";
    private static final String ATT4 = "att4";
    private static final String ATT5 = "att5";
    private static final String ATT6 = "att6";

    private static String totalLecturesVal = null;

    private FloatingActionButton saveStudentFAB;

    // Storage Permissions
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };




    ArrayList<HashMap<String, String>> jsonlist = new ArrayList<HashMap<String, String>>();

    JSONArray json = new JSONArray();


    ListView lv;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        new ProgressTask(this).execute();

        // set FloatingActionButton's event listener
        saveStudentFAB = (FloatingActionButton) findViewById(
                R.id.saveFloatingActionButton);
        saveStudentFAB.setOnClickListener(saveStudentButtonClicked);


    }

    // responds to event generated when user saves a contact
    private final View.OnClickListener saveStudentButtonClicked =
            new View.OnClickListener() {

                //Handler bound to the main thread
                final Handler mHandler = new Handler();

                @Override
                public void onClick(View v) {

                    Thread thread = new Thread() {
                        public void run() {

                            mHandler.post( new Runnable() {
                                public void run() {

                                    saveStudentReportToXls();

                                }
                            });
                        }
                    };

                    thread.start();
                }
            };

    private void saveStudentReportToXls() {

     //   new saveStudentReportToXlsTask().execute();

        File fileName = null;

        File StudentReportFolder = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_MOVIES);

        fileName = new File(StudentReportFolder, "/" + "StudentReport.xls");




//        if (!StudentReportFolder.exists()) {
//            boolean result = false;
//
//            try{
//                StudentReportFolder.mkdir();
//                System.out.println("==============>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+StudentReportFolder);
//                result = true;
//
//                try {
//                    fileName = new File("StudentReport.xls");
//                    fileName.createNewFile();
//                } catch (Exception ex) {
//                    System.out.println("ex: " + ex);
//                }
//
//            }
//            catch(SecurityException se){
//                Toast.makeText(getApplicationContext(),"Cant create output Directory",Toast.LENGTH_SHORT).show();
//            }
//            if(result) {
//                Toast.makeText(getApplicationContext(),"DIR created",Toast.LENGTH_SHORT).show();
//            }
//        }

        if (!isExternalStorageAvailable() || isExternalStorageReadOnly()) {
            Toast.makeText(getApplicationContext(),"External storage unavailable",Toast.LENGTH_SHORT).show();
        }

        ArrayList<StudentReport> stdRprt= createStdRprt(json);

        try {
            verifyStoragePermissions(this);
            generateExcel(stdRprt, fileName);
            Toast.makeText(getApplicationContext(),"Excel generated",Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(),"Excel generation failed",Toast.LENGTH_SHORT).show();
        }



    }

    /**
     * Checks if the app has permission to write to device storage
     *
     * If the app does not has permission then the user will be prompted to grant permissions
     *
     * @param activity
     */
    public static void verifyStoragePermissions(Activity activity) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
    }


//    private class saveStudentReportToXlsTask extends AsyncTask<Void, Void, Void> {
//
//        private Context context;
//
//        public saveStudentReportToXlsTask() {
//            this.context = context;
//        }
//
//        @Override
//        protected Void doInBackground(Void... params) {
//
//            File fileName = null;
//            File StudentReportFolder = new File(Environment.getExternalStorageDirectory(), "StudentReport");
//            if (!StudentReportFolder.exists()) {
//                boolean result = false;
//
//                try{
//                    StudentReportFolder.mkdir();
//                    result = true;
//
//                    try {
//                        fileName = new File(StudentReportFolder, "StudentReport" + ".xls");
//                        fileName.createNewFile();
//                    } catch (Exception ex) {
//                        System.out.println("ex: " + ex);
//                    }
//
//                }
//                catch(SecurityException se){
//                    Toast.makeText(getApplicationContext(),"Cant create output Directory",Toast.LENGTH_SHORT).show();
//                }
//                if(result) {
//                    Toast.makeText(getApplicationContext(),"DIR created",Toast.LENGTH_SHORT).show();
//                }
//            }
//
//            if (!isExternalStorageAvailable() || isExternalStorageReadOnly()) {
//                Toast.makeText(getApplicationContext(),"External storage unavailable",Toast.LENGTH_SHORT).show();
//            }
//
//            ArrayList<StudentReport> stdRprt= createStdRprt(json);
//
//            try {
//                generateExcel(stdRprt, fileName);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//
//
//            return null;
//        }
//    }

    private static boolean isExternalStorageReadOnly() {
        String extStorageState = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(extStorageState)) {
            return true;
        }
        return false;
    }

    public static boolean isExternalStorageAvailable() {
        String extStorageState = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(extStorageState)) {
            return true;
        }
        return false;
    }



    private class ProgressTask extends AsyncTask<String, Void, Boolean>{


        private ListActivity activity;

        private Context context;


        public ProgressTask(ListActivity activity) {

            this.activity = activity;
            context = activity;

        }

        @Override
        protected void onPostExecute(Boolean result){

            ListAdapter adapter = new SimpleAdapter(context, jsonlist, R.layout.list_item,
                                      new String[] {DATE, STUDENTID, STUDENTNAME, ACADEMICYEAR, SEMISTER, TOTALLECTURES, SUBJ1, SUBJ2, SUBJ3, SUBJ4, SUBJ5, SUBJ6,
                                                    ATT1, ATT2,ATT3, ATT4, ATT5, ATT6, FINALTOTALLECTURES
                                                        },
                                      new int[]{R.id.dateVal, R.id.studentIdVal, R.id.studentNameVal, R.id.academicYearVal, R.id.semisterVal, R.id.totalLecturesVal,
                                              R.id.subj1, R.id.subj2, R.id.subj3, R.id.subj4, R.id.subj5, R.id.subj6,
                                              R.id.att1, R.id.att2, R.id.att3, R.id.att4, R.id.att5, R.id.att6,
                                              R.id.finalTotalLecturesVal
                                                    }

                                                    );

            setListAdapter(adapter);

            final TextView textViewToChange = (TextView) findViewById(R.id.finalTotalLecturesVal);
            textViewToChange.setText(totalLecturesVal);

            lv = getListView();

        }




        @Override
        protected Boolean doInBackground(String... strings) {

            JSONParser jParser = new JSONParser();

          //  JSONArray json = jParser.getJSONFromUrl("https://api.myjson.com/bins/564fh");
            json = jParser.getJSONFromUrl("https://api.myjson.com/bins/2cxu5");

            for(int i = 0; i< json.length(); i++){

                HashMap<String, String> map = new HashMap<String, String>();

                try {


                        JSONObject c = json.getJSONObject(i);

                        if (c.length() <=1){

                            totalLecturesVal = c.get(TOTALLECTURES).toString();

                            continue;

                        }

                            String dateVal = c.getString(DATE);
                            String studentIdVal = c.getString(STUDENTID);
                            String studentNameVal = c.getString(STUDENTNAME);
                            String academicYearVal = c.getString(ACADEMICYEAR);
                            String semisterVal = c.getString(SEMISTER);
                            String totalLecturesVal = c.getString(TOTALLECTURES);

                            map.put(DATE, dateVal);
                            map.put(STUDENTID, studentIdVal);
                            map.put(STUDENTNAME, studentNameVal);
                            map.put(ACADEMICYEAR, academicYearVal);
                            map.put(SEMISTER, semisterVal);
                            map.put(TOTALLECTURES, totalLecturesVal);


                            JSONArray subjects = c.getJSONArray(SUBJECTS);

                            JSONArray attendance = c.getJSONArray(ATTENDANCE);

                            map.put(SUBJ1, subjects.getString(0));
                            map.put(SUBJ2, subjects.getString(1));
                            map.put(SUBJ3, subjects.getString(2));
                            map.put(SUBJ4, subjects.getString(3));
                            map.put(SUBJ5, subjects.getString(4));
                            map.put(SUBJ6, subjects.getString(5));


                            map.put(ATT1, attendance.getString(0));
                            map.put(ATT2, attendance.getString(1));
                            map.put(ATT3, attendance.getString(2));
                            map.put(ATT4, attendance.getString(3));
                            map.put(ATT5, attendance.getString(4));
                            map.put(ATT6, attendance.getString(5));
                            //       map.put(ATTENDANCE, attendanceVal);

                    jsonlist.add(map);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

        return false;
        }



    }
}
